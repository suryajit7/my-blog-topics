package functional.gui.modules;

import functional.BaseTest;
import org.testng.annotations.Test;

public class HomePageTest extends BaseTest {


    @Test(priority = 0)
    public void verifyNavigationToHome(){

        dashboardPage.getAllPieChartLabels();
        homePage.isPageLoaded()
                .click(homePage.getOrangeHRMLogo())
                .waitForPageToLoad();
    }
}
