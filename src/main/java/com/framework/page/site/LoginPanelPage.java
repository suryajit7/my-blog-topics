package com.framework.page.site;

import com.framework.page.BasePage;
import lombok.Getter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static com.framework.data.Constants.APP_URL;
import static com.framework.page.site.HomePage.ORANGE_HRM_LOGO;
import static com.framework.util.Await.*;

@Getter
public class LoginPanelPage extends BasePage {

    private static final String USERNAME_FIELD = "//input[@placeholder='Username']";
    private static final String PASSWORD_FIELD = "//input[@placeholder='Password']";
    private static final String LOGIN_BUTTON = "//button[normalize-space()='Login']";

    @FindBy(xpath = USERNAME_FIELD)
    private WebElement usernameField;

    @FindBy(xpath = PASSWORD_FIELD)
    private WebElement passwordField;

    @FindBy(xpath = LOGIN_BUTTON)
    private WebElement loginButton;

    public LoginPanelPage(WebDriver driver) {
        super(driver);
    }


    public LoginPanelPage goToAppLoginPage() {
        if (isUserLoggedIn()) {
            click(new HomePage(driver).getOrangeHRMLogo());
            logger.info("Skipping navigating to Login screen.");
        } else {
            goTo(APP_URL);
        }
        return this;
    }


    public Boolean isUserLoggedIn(){
        waitForPageToLoad();
        return isElementLoaded(By.xpath(ORANGE_HRM_LOGO));
    }


    @Override
    public LoginPanelPage isPageLoaded() {
        waitForPageToLoad();
        awaitUntil(elementIsDisplayed, usernameField);
        awaitUntil(elementIsDisplayed, passwordField);
        awaitUntil(elementIsDisplayed, loginButton);
        return this;
    }
}
