package com.framework.util;

import org.openqa.selenium.WebDriver;
import org.slf4j.LoggerFactory;
import org.testng.*;

import static com.assertthat.selenium_shutterbug.core.Capture.VIEWPORT;
import static com.assertthat.selenium_shutterbug.core.Shutterbug.shootPage;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.framework.data.Constants.FAILED_SCREENSHOTS_DIR_PATH;
import static java.lang.String.valueOf;

public class TestListener implements ITestListener, IInvokedMethodListener {

    protected static final org.slf4j.Logger logger = LoggerFactory.getLogger(TestListener.class);

    protected static final ThreadLocal<ITestNGMethod> currentMethods = new ThreadLocal<>();
    protected static final ThreadLocal<ITestResult> currentResults = new ThreadLocal<>();

    private static final String WEBDRIVER = "WebDriver";
    protected static final ThreadLocal<WebDriver> webdriver = new ThreadLocal<>();

    public static ITestNGMethod getTestMethod() {
        return checkNotNull(currentMethods.get(),
                "Did you forget to register the %s listener?", TestListener.class.getName());
    }

    /**
     * Parameters passed from a data provider are accessible in the test result.
     */
    public static ITestResult getTestResult() {
        return checkNotNull(currentResults.get(),
                "Did you forget to register the %s listener?", TestListener.class.getName());
    }


    @Override
    public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {
        currentMethods.set(method.getTestMethod());
        currentResults.set(testResult);
    }


    @Override
    public void afterInvocation(IInvokedMethod method, ITestResult testResult) {
        currentMethods.remove();
        currentResults.remove();
    }


    @Override
    public void onTestStart(ITestResult result) {

        logger.info("************************");
        logger.info(result.getMethod().getMethodName());
        logger.info("Test Priority:"+result.getMethod().getPriority());
    }


    @Override
    public void onTestSuccess(ITestResult result) {

        ITestContext context = result.getTestContext();

        logger.info("Test Status: Passed");
        logger.info("************************");
    }


    @Override
    public void onTestFailure(ITestResult result) {

        ITestContext context = result.getTestContext();
        if (context.getName().equalsIgnoreCase("UI Regression")){
            webdriver.set((WebDriver) context.getAttribute(WEBDRIVER));
            takeScreenshot(result, FAILED_SCREENSHOTS_DIR_PATH);
        }

        logger.info("Test Status: Failed");
        logger.error(result.getThrowable().getMessage());
        logger.info("************************");

    }


    @Override
    public void onTestSkipped(ITestResult result) {

        logger.info("Test Status: Skipped");
        logger.trace(valueOf(result.getSkipCausedBy()));
        logger.error(result.getThrowable().getMessage());
        logger.info("************************");
    }


    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
        logger.info("************************");
    }


    @Override
    public void onStart(ITestContext context) {

        logger.info("************************");
        logger.info("Test Suite Started...");
        logger.info(context.getSuite().getName());
        logger.info(valueOf(context.getSuite().getXmlSuite()));
        logger.info("************************");
    }


    @Override
    public void onFinish(ITestContext context) {
        logger.info("************************");
    }


    public void takeScreenshot(ITestResult result, String directoryPath){
            shootPage(webdriver.get(), VIEWPORT,true)
                    .withName(result.getMethod().getMethodName())
                    .save(directoryPath);
    }
}

